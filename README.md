# dijkstra C++

Lisez les commentaires du code, normalement tout va bien.

## hein ?
[Wikipedia Dijkstra](https://fr.wikipedia.org/wiki/Algorithme_de_Dijkstra)

## je teste comment ?
* `g++ main.cpp -o main`
* `./main`
* créez votre graphe
   * entrez votre graphe à la main (tout est indiqué)
   * ou alors copiez le contenu de  `data1.graph` (`ctrl+a` `ctrl+c` `ctrl+maj+v` dans le terminal)
* magic stuff

## data1.graph ?
Oui, c'est les données du graphe suivant:
![graphe](http://l3m.in/p/up/files/1522394022.png)