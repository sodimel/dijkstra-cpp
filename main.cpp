// g++ main.cpp -o main -std=c++11
// ./main
#include <iostream>
#include "Graph.cpp"

using namespace std;

int main(){

	Graph g;

	cout << "Entrez l'index de vos nœuds (0 pour arrêter):" << endl;
	int node(1);
	while(node != 0){
		cout << ">>";
		cin >> node;
		if(node != 0){
			if(g.addNode(node))
				cout << ">>>> le nœud " << node << " a bien été ajouté" << endl;
			else
				cout << ">>>> erreur lors de l'ajout du nœud " << node << endl;
		}
	}

	cout << "Entrez vos arcs avec la forme depuis,vers,distance, (entrez 0 sur \"depuis\" pour arrêter):" << endl;
	int depuis(1), vers, distance;
	while(depuis != 0){
		cout << "depuis>>";
		cin >> depuis;

		if(depuis != 0){
			cout << "vers>>";
			cin >> vers;
			cout << "distance>>";
			cin >> distance;

			if(g.addArc(depuis,vers,distance))
				cout << ">> l'arc " << depuis << "->" << vers << "(" << distance << ") a bien été ajouté" << endl;
			else
				cout << ">> erreur lors de l'ajout de l'arc " << depuis << "->" << vers << "(" << distance << ")" << endl;
		}
	}

	g.display();
	cout << "\n" << endl;

	cout << "Entrez le trajet à calculer avec la forme from->to (0 sur \"from\" pour arrêter):" << endl;
	int from(1), to, d;
	while(from != 0){
		cout << "from>>";
		cin >> from;

		if(from != 0){
			cout << "to>>";
			cin >> to;

				d = g.MooreDijkstra(from,to);

				cout << "\npcc:\n>> " << d << endl;

				cout << "\nshortest path:" << endl;
				
				for(int i(0); i<to; i++){
					g.display_shortest_paths(i);
					cout << "\n" << endl;
				}

		}
	}

	cout << "Bye." << endl;

	return 0;
}