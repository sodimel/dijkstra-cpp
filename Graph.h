#ifndef GRAPHCLASS_H
#define GRAPHCLASS_H

	#include <vector>

	class Graph{
		// liste de nœuds (index != numéro du nœud)
		std::vector<int> node;

		// liste d'arcs composés de from, to, distance, dispo
		std::vector<std::vector<int>> arc;

		// liste des pères des nœuds retenus
		std::vector<int> pere;

		// d[i] = distance pour arriver au nœud i
		std::vector<int> d;

		// debug, mettre à true et recompiler pour un output des étapes dans le terminal
		bool verbose = true;

		public:
			Graph();
			bool addNode(int);
			bool addArc(int, int, int);
			void display();
			int min(int, int);
			int MooreDijkstra(int, int);
			void minArcDispo(int*, int*, int*);
			int getIdByNode(int);
			int getNodeById(int);
			void setFutureNodesToTrue(int);
			void display_shortest_paths(int);
	};

#endif
