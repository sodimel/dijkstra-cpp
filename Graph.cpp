#define MAX_VAL 1000000

#include <iostream>
#include "Graph.h"

using namespace std;

Graph::Graph(){
	
}

// ajoute un nœud
bool Graph::addNode(int n){
	for(int i(0); i<node.size(); i++){
		if(node[i] == n)
			return false;
	}
	node.push_back(n);
	pere.push_back(MAX_VAL);
	d.push_back(MAX_VAL);
	return true;
}


// ajoute un arc
bool Graph::addArc(int n1, int n2, int d){
	bool exist1 = false, exist2 = false;

	// si l'arc existe déjà
	for(int i(0); i<arc.size(); i++){
		if(arc[i][0] == getIdByNode(n1) && arc[i][1] == getIdByNode(n2))
			return false;
	}

	// vérifie si les nœuds existent
	for(int i(0); i<node.size(); i++){
		if(node[i] == n1)
			exist1 = true;
		if(node[i] == n2)
			exist2 = true;
	}

	// on ajoute l'arc
	if(exist1 && exist2){
		arc.push_back({getIdByNode(n1),getIdByNode(n2),d,0});
		return true;
	}
	else
		return false;
}


// affichage simpliste du graphe
void Graph::display(){
	cout << "\n\nDisplay graph:\n" << endl;
	for(int i(0); i<node.size(); i++){
		cout << "id" << i << "[" << node[i] << "]\n";
		for(int j(0); j<arc.size(); j++){
			if(arc[j][0] == getIdByNode(node[i]))
				cout << "  ->id" << getIdByNode(arc[j][1]) << "[" << arc[j][1] << "](" << arc[j][2] << ")\n";
		}
	}
}


// gloubi boulga infâme (c'est une homme)
int Graph::MooreDijkstra(int debut, int fin){
	
	// initialisation
	int current(0);

	current = getIdByNode(debut); // on chope l'id du nœud de début
	fin = getIdByNode(fin); // on chope l'id du nœud de fin

	d[current] = 0; // distance du début = 0
	pere[current] = current; // père du début = début

	// on initialise les arcs à visiter qui vont de début à un autre nœud
	setFutureNodesToTrue(current);

	int p, newd;
	// p = stockage temporaire du père
	// newd = stockage temporaire de la distance de p à current

	// tant qu'on explore pas le dernier nœud on continue d'explorer
	// faudrait vérifier qu'on puisse rejoindre fin depuis début, mais on verra ça une autre fois
	for(int aa(0); aa<node.size(); aa++){
		if(verbose)
			cout << "----\n-current: " << current << "\n-d[" << current << "]: " << d[current] << "\n-pere[" << current << "]: " << pere[current] << endl;
		
		// on change de nœud et on mets les arcs qui en partent sur la liste des arcs à visiter, et on màj la distance pour aller au nœud changé
		minArcDispo(&current, &p, &newd);

		// si cet itinéraire est plus rapide que MAX_INT ou que celui qu'on a déjà fait avant
		if(d[current] > newd){
			pere[current] = p; // on change le papa
			d[current] = newd; // et on remplace la distance
		}

		//
		// Et c'est tout. Wala.
		//

		if(verbose)
			cout << "--newcurrent:" << current << "\n  pere:" << pere[current] << endl;

		if(verbose)
			cout << "\n-current: " << current << "\n-d[" << current << "]: " << d[current] << "\n-pere[" << current << "]: " << pere[current] << endl;
	}


	if(verbose){
			cout << "\n\n-----\nFINI\n-----\n\n\nTrajet:" << endl;
		for(int i(fin); i!=getIdByNode(debut); i = pere[i]){
			cout << getNodeById(i) << "->" << getNodeById(pere[i]) << "(" << d[i] - d[pere[i]] << ")" << endl;
		}
	}

	return d[fin]; // là on renvoie la distance de la fin parce que c'est ce qu'on veut
}


// prend la plus petite distance des arcs qui sont à visiter
void Graph::minArcDispo(int *current, int *pere, int *newd){

	if(verbose)
			cout << "--minArcDispo\n  arcs availables:" << endl;

	// sauvegarde de distance, from, to, indexArc
	int min[4] = {MAX_VAL};

	// exploration des arcs
	for(int i(0); i<arc.size(); i++){
		if(arc[i][3] == 1){

			if(verbose)
				cout << "  " << arc[i][0] << "->" << arc[i][1] << "(" << arc[i][2] << ")" << endl;

			if(arc[i][2] < min[0]){
				min[0] = arc[i][2]; // distance
				min[1] = arc[i][0]; // id from
				min[2] = arc[i][1]; // id to
				min[3] = i;

				if(verbose)
					cout << "  --select: " << arc[min[3]][0] << "->" << arc[min[3]][1] << "(" << min[0] << ")" << endl;

			}
		}
	}

	if(verbose)
		cout << "  selected: "<< arc[min[3]][0] << "->" << arc[min[3]][1] << "(" << min[0] << ")\n--end" << endl;

	// cet arc n'est plus à visiter
	arc[min[3]][3] = 2;

	// en revanche ceux qui partent de ce nouveau nœud oui
	setFutureNodesToTrue(min[2]);
	
	// on change les vars
	*current = min[2]; // to
	*pere = min[1]; // from
	// distance
	*newd = d[*pere]+min[0];

}


// ajoute les arcs partant de n sur la liste de ceux à visiter
void Graph::setFutureNodesToTrue(int n){

	if(verbose)
		cout << "--setFutureNodesToTrue" << endl;

	// exploration des arcs
	for(int i(0); i<arc.size(); i++){
		if(arc[i][0] == n && arc[i][3] != 2){ // id from == id nœud ?

			if(verbose)
				cout << "  " << arc[i][0] << "->" << arc[i][1] << endl;
			
			arc[i][3] = 1;
		}
	}

	if(verbose)
		cout << "--end\n" << endl;

}


// réupère l'id du noeud passé en argument
int Graph::getIdByNode(int n){
	for(int i(0); i<node.size(); i++)
		if(node[i] == n)
			return i;
}


// récupère le nom du nœud passé en argument
int Graph::getNodeById(int id){
	return node[id];
}


// affiche les chemins les plus courts de p au début
// récursif
void Graph::display_shortest_paths(int p){
	if(pere[pere[p]] != pere[p])
		display_shortest_paths(pere[p]);
	cout << node[pere[p]] << "->" << node[p] << ": " << d[p] - d[pere[p]] << " (" << d[p] << ")" << endl;
}